#!/bin/bash
PURPOSE="installs ansible dependencies for debian/rhel based os (offline installation as failback)"
VERSION="2014-08-16-001"
AUTHOR="fidy@andrianaivo.org"

# automatic variables
# -- about current script
scriptSelf=$0
scriptName=${scriptSelf##*/}
scriptCallDir=${scriptSelf%/*};cd $scriptCallDir;
scriptFullDir=$PWD
scriptFullPath=$scriptFullDir/$scriptName
scriptLogFile=/tmp/${scriptName}.log
scriptParameters=""
# -- about current user
currentUsername=$USER


# script requirements
# -- list of executables used by the script #
scriptExternalCommands="date"
# -- force script to be run as the given user
scriptRunasUsername=


# ------ script usage ----- #
read -d '' USAGE <<daer
	Usage: $scriptName [-f|--force] [-o|--offline-only] -i|--install
daer
# ------ /script usage ----- #


# script functions
function printLog	{ echo -e "$(date -Is) $@" >>$scriptLogFile; }
function printOut	{ echo -e "$@"; printLog "$@"; }
function printUsage	{ echo -e "# $PURPOSE\n# version $VERSION ($AUTHOR)\n\n$USAGE\n"; exit 0; }
function exitError 	{ msg="[$(hostname)] ERROR: $1. Exiting..."; printOut "$msg"; printLog "$msg"; exit 1; }
function showPrompt	{ if [[ -n $noPrompt && $noPrompt = true ]];then printOut "* Running in force mode (no prompt)"; else echo -e "* Press ENTER to continue or CTRL+C to cancel!"; read a; fi }
function checkCommands 	{ for bin in $scriptExternalCommands; do [[ -x "$(which $bin)" ]] || exitError "the binary [$bin] is required but not found"; done }
function checkUsername 	{ 
if [[ -n "$scriptRunasUsername" && "$currentUsername" != "$scriptRunasUsername" ]];then 
	echo -e "=> wrong username [$currentUsername]. Switching to user [$scriptRunasUsername]..."; 
	sudo su $scriptRunasUsername -s /bin/bash -c "$scriptFullPath $scriptParameters"; 
	exit 0; 
fi }


# ------ parameters parsing  ----- #
# no parameter
[[ -z "$1" ]] && printUsage
# walk through all parameters
while [[ -n "$1" ]]
do
	# evaluate parameters (avoid runing commands here
	# 'cause username is not yet verified.
	case "$1" in
		-f|--force)
			noPrompt=true
			;;
		-o|--offline-only)
			offlineOnly=true
			;;
		-i|--install)
			install=true
			;;
		*)
			printUsage
			;;
	esac
	scriptParameters+=" $1"
	shift
done
# ------ /parameters parsing  ----- #




# ----- the main script starts here #
# -- confirm script start
[[ -z "$install" ]] && printUsage

# -- check required username
checkUsername

# -- initialise logfile
echo -e "Script name: $scriptFullPath\nLast run: $(date)\nParameters: $scriptParameters\n--" >$scriptLogFile
printLog $scriptParameters

# -- supported package managers
packageManagers="apt yum"
# - options for apt
packageInstaller_apt="dpkg -i"
packageList_apt="python-crypto python-httplib2 python-jinja2 python-markupsafe python-yaml"
# - options for yum
packageInstaller_yum="rpm -ih"
packageList_yum="python-babel python-jinja2 python-yaml"

# -- check os package manager 
for pm in $packageManagers; 
do 
	[[ -x "$(which $pm)" ]] && PM=$pm || continue 	# test for available package manager
	scriptExternalCommands+=" $pm" 			# add detected PM to our requirements
	installDir="/tmp/dependencies/$pm"		# set temp install dir containing offline packages
	installCmdStr="packageInstaller_${pm}"	
	installCmd=${!installCmdStr}			# set install command to use for offline install
done

# -- check commands
checkCommands

# -- 
printOut "* extracting offline packages..."
showPrompt
packageBaseDir=${scriptFullDir%/*};
offlinePackage=$packageBaseDir/lib/dependencies.tgz
[[ -f $offlinePackage ]] || exitError "offline package file [$offlinePackage] not found."
tar -C /tmp -xzf $offlinePackage || exitError "could not extract offline package file [$offlinePackage]."
printOut "=> packages available at [$installDir].\n"

# -- 
printOut "* installing offline packages..."
showPrompt
$installCmd $installDir/* >>$scriptLogFile || exitError "could not install offline package file [$offlinePackage]."
printOut "=> offline packages installed from [$installDir].\n"

# -- 
printOut "* checking installation & setting environment..."
showPrompt
environmentFile=$packageBaseDir/cfg/ansible.env
rootProfile=/root/.profile
[[ -f $environmentFile ]] || exitError "Environment file [$environmentFile] not found. Please check package installation under [$packageBaseDir]."
grep -q "$environmentFile" $rootProfile || echo -e "\n# source ansible environment\nsource $environmentFile\n" >>$rootProfile || exitError "could not edit [$rootProfile]."
source $environmentFile
ansible all -a hostname  || exitError "could not test ansible installation."
printOut "=> ansible package installed successfully.\n"


exit 0
# ----- end of file (but not of life)