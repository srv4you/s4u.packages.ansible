# S4U ansible package #

This is a generic ansible offline installation package for s4u managed servers. It can be used as a package for our [generic package installer](https://bitbucket.org/srv4you/s4u.packages.bootstrap), but also as a standalone deployment package


### Getting Started ###

clone or unpack the package content into a directory (ex. /opt/ansible)
```
#!shell
git clone https://andrianaivo@bitbucket.org/srv4you/s4u.packages.ansible.git /opt/ansible
```

run the installer script in the **bin** directory:
```
#!shell
/opt/ansible/bin/ansible.bootstrap.sh 
```

you can now start using ansible (put your configfiles accordingly in the directories under the **cfg** path:
```
#!shell
ansible all --list-hosts
ansible all -m ping
```



### Supported OS platforms ###

* Ubuntu 12.04 or later
* Debian 6 or later
* RHEL/CentOS 6 or later